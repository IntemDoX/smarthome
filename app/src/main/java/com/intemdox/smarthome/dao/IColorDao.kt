package com.intemdox.smarthome.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.intemdox.smarthome.entity.ColorEntity

@Dao
interface IColorDao {
  @Insert
  fun insert(color: ColorEntity)

  @Delete
  fun delete(color: ColorEntity)

  @Query("DELETE FROM color_table")
  fun deleteAll()

  @Query("SELECT * FROM color_table WHERE type=0")
  fun getSavedColors(): LiveData<List<ColorEntity>>

  @Query("SELECT * FROM color_table WHERE type=1")
  fun getRecentColors(): LiveData<List<ColorEntity>>
}