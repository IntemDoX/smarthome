package com.intemdox.smarthome.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.intemdox.smarthome.entity.BTDeviceEntity

@Dao
interface IBTDevicesDao {
  @Insert
  fun insert(color: BTDeviceEntity)

  @Delete
  fun delete(color: BTDeviceEntity)

  @Query("DELETE FROM btdevices_table")
  fun deleteAll()

  @Query("SELECT * FROM btdevices_table")
  fun getAllDevices(): LiveData<List<BTDeviceEntity>>
}