package com.intemdox.smarthome.dagger2.components

import com.intemdox.smarthome.ColorsAdapter
import com.intemdox.smarthome.dagger2.modules.ColorsAdapterModule
import dagger.Component

@Component(modules = [ (ColorsAdapterModule::class)])
interface IColorAdapterComponent {
  fun getColorsAdapter(): ColorsAdapter
}