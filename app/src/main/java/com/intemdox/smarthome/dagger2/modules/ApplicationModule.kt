package com.intemdox.smarthome.dagger2.modules

import android.app.Application
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule(val application: Application) {
  @Provides
  fun provideApp(): Application {
    return application
  }
}