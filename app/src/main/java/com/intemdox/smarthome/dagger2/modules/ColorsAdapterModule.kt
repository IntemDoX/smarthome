package com.intemdox.smarthome.dagger2.modules

import com.intemdox.smarthome.entity.ColorEntity
import dagger.Module
import dagger.Provides

@Module
class ColorsAdapterModule(private val colorsList: List<ColorEntity>) {
  @Provides
  fun provideColorsList(): List<ColorEntity> {
    return colorsList
  }
}