package com.intemdox.smarthome.dagger2.components

import com.intemdox.smarthome.ColorsAdapter
import com.intemdox.smarthome.DevicesAdapter
import com.intemdox.smarthome.dagger2.modules.ColorsAdapterModule
import com.intemdox.smarthome.dagger2.modules.DevicesAdapterModule
import dagger.Component

@Component(modules = [(DevicesAdapterModule::class)])
interface IAdapterComponent {
  fun getDevicesAdapter(): DevicesAdapter
}