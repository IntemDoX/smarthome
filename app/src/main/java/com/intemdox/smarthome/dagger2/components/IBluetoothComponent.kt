package com.intemdox.smarthome.dagger2.components

import com.intemdox.smarthome.BluetoothHelper
import com.intemdox.smarthome.dagger2.modules.ApplicationModule
import dagger.Component

@Component(modules = [ApplicationModule::class])
interface IBluetoothComponent {
  fun getBluetoothHelper(): BluetoothHelper
}