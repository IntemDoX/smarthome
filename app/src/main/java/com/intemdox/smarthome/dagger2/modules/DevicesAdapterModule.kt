package com.intemdox.smarthome.dagger2.modules

import com.intemdox.smarthome.entity.BTDeviceEntity
import dagger.Module
import dagger.Provides

@Module
class DevicesAdapterModule(private val devicesList: List<BTDeviceEntity>) {
  @Provides
  fun provideDevicesList(): List<BTDeviceEntity> {
    return devicesList
  }
}