package com.intemdox.smarthome.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity(tableName = "color_table")
class ColorEntity(@NonNull @ColumnInfo(name = "color") var color: Int, @NonNull @ColumnInfo(name = "type") var type: Int) {
  @PrimaryKey(autoGenerate = true)
  var id: Int = 0


  override fun equals(other: Any?): Boolean {
    if (other is ColorEntity) {
      return color == other.color
    }
    return false
  }

  override fun hashCode(): Int {
    var result = id
    result = 31 * result + color
    result = 31 * result + type
    return result
  }
}