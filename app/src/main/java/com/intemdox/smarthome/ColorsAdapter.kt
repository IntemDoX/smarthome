package com.intemdox.smarthome

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.intemdox.smarthome.entity.ColorEntity
import de.hdodenhof.circleimageview.CircleImageView
import javax.inject.Inject


class ColorsAdapter @Inject constructor(private val colorsList: List<ColorEntity>) : RecyclerView.Adapter<ColorsAdapter.ViewHolder>() {

    lateinit var onItemClickListener: OnItemClickListener

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorsAdapter.ViewHolder {
    val view = LayoutInflater.from(parent.context).inflate(R.layout.item_color, parent, false)
    return ViewHolder(view)
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    holder.colorIndicator.circleBackgroundColor = colorsList[position].color
    holder.ccc.setBackgroundColor(colorsList[position].color)
    holder.itemView!!.setOnClickListener({
      onItemClickListener.setItemColor(holder.colorIndicator.circleBackgroundColor)
    })
  }

  override fun getItemCount(): Int {
    return colorsList.size
  }

  class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val colorIndicator = view.findViewById<CircleImageView>(R.id.color_indicator)!!
    val ccc = view.findViewById<ImageView>(R.id.ccc)!!
  }

  interface OnItemClickListener {
    fun setItemColor(color: Int)
  }

}