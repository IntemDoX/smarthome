package com.intemdox.smarthome

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.graphics.ColorUtils
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.intemdox.smarthome.entity.ColorEntity
import com.intemdox.smarthome.dagger2.components.DaggerIColorAdapterComponent
import com.intemdox.smarthome.dagger2.modules.ColorsAdapterModule
import com.intemdox.smarthome.viewmodel.LedViewModel
import com.madrapps.pikolo.listeners.SimpleColorSelectionListener
import kotlinx.android.synthetic.main.fragment_led.*
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class LedFragment : Fragment(), ColorsAdapter.OnItemClickListener, View.OnClickListener {
  var colorrrr: Int = 0

  override fun onClick(v: View?) {
    when (v) {
      btn_save_color -> {
        val c = ColorEntity(colorrrr, 0)
        ledViewModel.saveColor(c)
      }
      btn_accept_color -> {
        val c = ColorEntity(colorrrr, 1)
        ledViewModel.acceptColor(c, true)
      }
      randomButton -> {
        val color = ColorUtils.HSLToColor(floatArrayOf(random.nextInt(360).toFloat(), random.nextFloat(), random.nextFloat()))
        val hexColor = String.format("#%06X", 0xFFFFFF and color)
        randomButton.text = hexColor
        imageView.background.setColorFilter(color, PorterDuff.Mode.MULTIPLY)
        colorPicker.setColor(color)
        ledViewModel.acceptColor(ColorEntity(colorrrr, 1), true)
      }
    }
  }

  override fun setItemColor(color: Int) {
    colorPicker.setColor(color)
    imageView.background.setColorFilter(color, PorterDuff.Mode.MULTIPLY)
    ledViewModel.acceptColor(ColorEntity(color, 0), false)
  }

  // TODO: Rename and change types of parameters
  private var param1: String? = null
  private var param2: String? = null
  private var listener: OnFragmentInteractionListener? = null

  private lateinit var ledViewModel: LedViewModel
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    arguments?.let {
      param1 = it.getString(ARG_PARAM1)
      param2 = it.getString(ARG_PARAM2)
    }
    ledViewModel = ViewModelProviders.of(this, LedViewModel.Factory(activity!!.application)).get(LedViewModel::class.java)
    ledViewModel.savedColorsLiveData.observe(this, Observer<List<ColorEntity>> {
    })
    ledViewModel.savedColorsMediator.addSource(ledViewModel.savedColorsLiveData, {
      if (it != null) {
        initSavedColorsAdapter(it)
      }
    })
    ledViewModel.savedColorsMediator.observe(this, Observer<List<ColorEntity>> {
      if (it != null) {
        initSavedColorsAdapter(it)
      }
    })

    ledViewModel.recentColorsLiveData.observe(this, Observer<List<ColorEntity>> {
    })
    ledViewModel.recentColorsMediator.addSource(ledViewModel.recentColorsLiveData, {
      if (it != null) {
        initRecentColorsAdapter(it)
      }
    })
    ledViewModel.recentColorsMediator.observe(this, Observer<List<ColorEntity>> {
      if (it != null) {
        initRecentColorsAdapter(it)
      }
    })
  }

  private fun initSavedColorsAdapter(savedColors: List<ColorEntity>) {
    if (savedColors.isEmpty()) {
      Log.d("testtest", getString(R.string.bt_no_devices_found_message))
      return
    }
    saved_colors.layoutManager = LinearLayoutManager(context, LinearLayout.HORIZONTAL, false)
    val savedColorsAdapter = DaggerIColorAdapterComponent.builder().colorsAdapterModule(ColorsAdapterModule(savedColors)).build()
    val adapter = savedColorsAdapter.getColorsAdapter()
    adapter.onItemClickListener = this
    saved_colors.adapter = adapter
    if (savedColors.isNotEmpty()) {
      saved_colors.visibility = View.VISIBLE
    }
  }

  private fun initRecentColorsAdapter(recentColors: List<ColorEntity>) {
    if (recentColors.isEmpty()) {
      Log.d("testtest", getString(R.string.bt_no_devices_found_message))
      return
    }
    recent_colors.layoutManager = LinearLayoutManager(context, LinearLayout.HORIZONTAL, false)
    val recentColorAdapter = DaggerIColorAdapterComponent.builder().colorsAdapterModule(ColorsAdapterModule(recentColors)).build()
    val adapter = recentColorAdapter.getColorsAdapter()
    adapter.onItemClickListener = this
    recent_colors.adapter = adapter
    if (recentColors.isNotEmpty()) {
      recent_colors.visibility = View.VISIBLE
    }
  }

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                            savedInstanceState: Bundle?): View? {
    return inflater.inflate(R.layout.fragment_led, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    btn_save_color.setOnClickListener(this)
    btn_accept_color.setOnClickListener(this)
    randomButton.setOnClickListener(this)
    colorPicker.setColorSelectionListener(object : SimpleColorSelectionListener() {
      override fun onColorSelected(color: Int) {
        imageView.background.setColorFilter(color, PorterDuff.Mode.MULTIPLY)
        colorrrr = color
      }
    })
  }

  // TODO: Rename method, update argument and hook method into UI event
  fun onButtonPressed(uri: Uri) {
    listener?.onFragmentInteraction(uri)
  }

  override fun onAttach(context: Context) {
    super.onAttach(context)
    if (context is OnFragmentInteractionListener) {
      listener = context
    } else {
      throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
    }
  }

  override fun onDetach() {
    super.onDetach()
    listener = null
  }

  interface OnFragmentInteractionListener {
    // TODO: Update argument type and name
    fun onFragmentInteraction(uri: Uri)
  }

  companion object {
    const val TAG = "LEDFragment"
    const val MODE_CHANGE_COLOR = 0
    const val MODE_PULSE_ONE_COLOR = 1
    //    lateinit var ledCommandListener: LEDCommandListener
    val random = Random()
    var mode = MODE_CHANGE_COLOR

    // TODO: Rename and change types and number of parameters
    @JvmStatic
    fun newInstance(param1: String, param2: String) =
        LedFragment().apply {
          arguments = Bundle().apply {
            putString(ARG_PARAM1, param1)
            putString(ARG_PARAM2, param2)
          }
        }
  }
}
