package com.intemdox.smarthome.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.intemdox.smarthome.entity.BTDeviceEntity
import com.intemdox.smarthome.entity.ColorEntity
import com.intemdox.smarthome.dao.IBTDevicesDao
import com.intemdox.smarthome.dao.IColorDao

@Database(entities = [(BTDeviceEntity::class), (ColorEntity::class)], version = 6)
abstract class AppDatabase : RoomDatabase() {
  abstract fun provideBTDeviceDao(): IBTDevicesDao
  abstract fun provideColorDao(): IColorDao

  companion object {
    private const val DB_NAME = "app_database"
    private var dbInstance: AppDatabase? = null

    fun getDatabase(context: Context): AppDatabase? {
      if (dbInstance == null) {
        dbInstance = Room.databaseBuilder<AppDatabase>(context.applicationContext, AppDatabase::class.java, DB_NAME).build()
      }
      return dbInstance
    }
  }
}