package com.intemdox.smarthome.exception

/**
 * Created by Vladislav Kavunenko
 * on 16.04.2018.
 */
class BTNotEnableException : Exception()