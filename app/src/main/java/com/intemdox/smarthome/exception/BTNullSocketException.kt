package com.intemdox.smarthome.exception

/**
 * Created by Vladislav Kavunenko
 * on 17.04.2018.
 */
class BTNullSocketException : Exception()