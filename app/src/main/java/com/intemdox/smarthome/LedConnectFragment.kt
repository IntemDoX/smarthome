package com.intemdox.smarthome

import android.arch.lifecycle.ViewModelProviders
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.navigation.Navigation
import com.intemdox.smarthome.exception.BTNotEnableException
import com.intemdox.smarthome.exception.BTNotExistException
import com.intemdox.smarthome.entity.BTDeviceEntity
import com.intemdox.smarthome.dagger2.components.DaggerIAdapterComponent
import com.intemdox.smarthome.dagger2.modules.DevicesAdapterModule
import com.intemdox.smarthome.viewmodel.LedConnectionViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_led_connect.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [LedFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [LedFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class LedConnectionFragment : Fragment(), View.OnClickListener, DevicesAdapter.OnItemClickListener {
  private val TAG: String = "LedConnectionFragment"
  override fun connectToDevice(macAddress: String) {
    ledConnectionViewModel.connectToDevice(macAddress)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .doOnError { Log.d(TAG, "connectToDevice Error " + it.message) }
        .subscribe({
          val navController = Navigation.findNavController(view!!)
          navController.navigate(R.id.ledFragment)
        }, {
          Log.d(TAG, "connectToDevice Error " + it.message)
        })
  }

  // TODO: Rename and change types of parameters
  private var param1: String? = null
  private var param2: String? = null
  private var listener: OnFragmentInteractionListener? = null


  private lateinit var ledConnectionViewModel: LedConnectionViewModel

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    arguments?.let {
      param1 = it.getString(ARG_PARAM1)
      param2 = it.getString(ARG_PARAM2)
    }

    ledConnectionViewModel = ViewModelProviders.of(this, LedConnectionViewModel.Factory(activity!!.application)).get(LedConnectionViewModel::class.java)
    ledConnectionViewModel.btDevicesLiveData.observe(this, android.arch.lifecycle.Observer<List<BTDeviceEntity>> {

    })
    ledConnectionViewModel.btDevicesMediator.addSource(ledConnectionViewModel.btDevicesLiveData, {
      if (it != null) {
        initAdapter(it)
      }
    })
    ledConnectionViewModel.btDevicesMediator.observe(this, android.arch.lifecycle.Observer<List<BTDeviceEntity>> {
      if (it != null) {
        initAdapter(it)
      }
    })
  }

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                            savedInstanceState: Bundle?): View? {
    return inflater.inflate(R.layout.fragment_led_connect, container, false)
  }

  override fun onClick(v: View?) {
    when (v) {
      btn_find_devices -> {
        if (ledConnectionViewModel.isRefresh(devices_list.adapter != null)) {
          ledConnectionViewModel.refreshDevices()
        } else {
          ledConnectionViewModel.findDevices().subscribe({
            ledConnectionViewModel.refreshDevices()
          }, {
            when (it) {
              is BTNotExistException -> showMessage(getString(R.string.bt_not_supported_message))
              is BTNotEnableException -> {
                val enableBluetoothIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableBluetoothIntent, MainActivity.REQUEST_ENABLE_BLUETOOTH)
              }
            }
          })
        }
      }
      btn_connect_available_devices -> {
        ledConnectionViewModel.connectToAllDevices().subscribe({
        })
      }
    }
  }

  private fun showMessage(text: String = "", isVisible: Boolean = true) {
    if (isVisible) {
      message.visibility = View.VISIBLE
      message.text = text
    } else {
      message.visibility = View.GONE
    }
  }

  private fun initAdapter(devicesList: List<BTDeviceEntity>) {
    if (devicesList.isEmpty()) {
      showMessage(getString(R.string.bt_no_devices_found_message))
      return
    }

    showMessage(isVisible = false)
    devices_list.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
    val deviceAdapterComponent = DaggerIAdapterComponent.builder().devicesAdapterModule(DevicesAdapterModule(devicesList)).build()
    val adapter = deviceAdapterComponent.getDevicesAdapter()
    adapter.onItemClickListener = this
    devices_list.adapter = adapter
    if (devicesList.isNotEmpty()) {
      devices_list.visibility = View.VISIBLE
    }
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    btn_find_devices.background.setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY)
    btn_find_devices.setOnClickListener(this)
  }

  // TODO: Rename method, update argument and hook method into UI event
  fun onButtonPressed(uri: Uri) {
    listener?.onFragmentInteraction(uri)
  }

  override fun onAttach(context: Context) {
    super.onAttach(context)
    if (context is OnFragmentInteractionListener) {
      listener = context
    } else {
      throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
    }
  }

  override fun onDetach() {
    super.onDetach()
    listener = null
  }

  /**
   * This interface must be implemented by activities that contain this
   * fragment to allow an interaction in this fragment to be communicated
   * to the activity and potentially other fragments contained in that
   * activity.
   *
   *
   * See the Android Training lesson [Communicating with Other Fragments]
   * (http://developer.android.com/training/basics/fragments/communicating.html)
   * for more information.
   */
  interface OnFragmentInteractionListener {
    // TODO: Update argument type and name
    fun onFragmentInteraction(uri: Uri)
  }

  companion object {
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LedFragment.
     */
    // TODO: Rename and change types and number of parameters
    @JvmStatic
    fun newInstance(param1: String, param2: String) =
        LedFragment().apply {
          arguments = Bundle().apply {
            putString(ARG_PARAM1, param1)
            putString(ARG_PARAM2, param2)
          }
        }
  }

}
