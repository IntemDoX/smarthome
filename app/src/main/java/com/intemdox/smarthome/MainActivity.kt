package com.intemdox.smarthome

import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class MainActivity : AppCompatActivity(), LedConnectionFragment.OnFragmentInteractionListener, LedFragment.OnFragmentInteractionListener {
  companion object {
    const val TAG = "MainActivity"
    const val REQUEST_ENABLE_BLUETOOTH = 1
    const val SEVERAL_DEVICES = "several_devices"
  }

  override fun onFragmentInteraction(uri: Uri) {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
  }
}