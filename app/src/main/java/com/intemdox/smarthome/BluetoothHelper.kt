package com.intemdox.smarthome

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.util.Log
import com.intemdox.smarthome.exception.BTNotEnableException
import com.intemdox.smarthome.exception.BTNotExistException
import com.intemdox.smarthome.exception.BTNullSocketException
import io.reactivex.Completable
import io.reactivex.Single
import java.io.IOException
import java.util.*
import javax.inject.Inject

class BluetoothHelper @Inject constructor() {
  companion object {
    const val TAG = "testtest"
    var myUUID: UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
    var btSocket: BluetoothSocket? = null
    var isConnected: Boolean = false
    const val EXTRA_ADDRESS: String = "Device_address"
  }

  private var btAdapter: BluetoothAdapter? = null
  private lateinit var pairedDevices: Set<BluetoothDevice>
  private lateinit var address: String
  private val KEY_PREF = "shared"
  private val KEY_MAC = "mac"

  fun enableBT(): Completable {
    return Completable.create {
      btAdapter = BluetoothAdapter.getDefaultAdapter()
      if (btAdapter == null) {
        if (!it.isDisposed) {
          it.onError(BTNotExistException())
        }
      }
      if (!btAdapter!!.isEnabled) {
        if (!it.isDisposed) {
          it.onError(BTNotEnableException())
        }
      } else {
        if (!it.isDisposed) {
          it.onComplete()
        }
      }
    }
  }


  fun sendCommand(input: IntArray): Completable {
    return Completable.create {
      if (btSocket != null) {
        try {
          for (i in input) {
            btSocket!!.outputStream.write(i)
          }
          if (!it.isDisposed) {
            Log.d(TAG, "sendCommand success")
            it.onComplete()
          }
        } catch (e: IOException) {
          if (!it.isDisposed) {
            Log.d(TAG, "sendCommand error " + e.message)
            it.onError(e)
          }
        }
      } else {
        try {
          val device: BluetoothDevice? = btAdapter?.getRemoteDevice(address)
          btSocket = device?.createInsecureRfcommSocketToServiceRecord(myUUID)
          for (i in input) {
            btSocket!!.outputStream.write(i)
          }
          it.onComplete()
        } catch (e: IOException) {
          if (!it.isDisposed) {
            Log.d(TAG, "sendCommand error " + e.message)
            it.onError(BTNullSocketException())
          }
        }
      }
    }
  }


  fun disconnect(): Completable {
    return Completable.create {
      if (btSocket != null) {
        try {
          btSocket!!.close()
          btSocket = null
          isConnected = false
          if (!it.isDisposed) {
            it.onComplete()
          }
        } catch (e: IOException) {
          if (!it.isDisposed) {
            it.onError(e)
          }
        }
      }
    }
  }


  fun connectToDevice(address: String): Completable {
    this.address = address
    return Completable.create {
      try {
        if (btSocket == null || !isConnected) {
          btAdapter = BluetoothAdapter.getDefaultAdapter()
          if (btAdapter == null) {
            it.onError(BTNotEnableException())
          } else {
            val device: BluetoothDevice? = btAdapter?.getRemoteDevice(address)
            btSocket = device?.createInsecureRfcommSocketToServiceRecord(myUUID)
            BluetoothAdapter.getDefaultAdapter().cancelDiscovery()
            btSocket!!.connect()
            if (!it.isDisposed) {
              it.onComplete()
            }
          }
        } else {
          btSocket!!.connect()
          if (!it.isDisposed) {
            it.onComplete()
          }
        }
      } catch (e: IOException) {
        if (!it.isDisposed) {
          it.onError(e)
        }
      }
    }
  }

  fun connectToAllDevices(): Completable {
    return Completable.create {
      val list = getDevicesList()
      for (address in list) {
        connectToDevice(address).subscribe({
        })
      }
      if (!it.isDisposed) {
        it.onComplete()
      }
    }
  }

  fun saveDevice(mac: String) {
    val list = getDevicesList()
    if (!list.contains(mac)) {
      list.add(mac)
    }
    //TODO save to room6
  }

  private fun getDevicesList(): ArrayList<String> {
    //TODO getDevices from room
    return ArrayList()
  }


  fun isAdapterEnable(): Boolean {
    val isAdapterEnabled = btAdapter?.isEnabled
    return btAdapter != null && isAdapterEnabled != null && isAdapterEnabled
  }

  fun getPairedDeviceList(): Single<ArrayList<BluetoothDevice>> {
    return Single.create {
      pairedDevices = btAdapter!!.bondedDevices
      val list: ArrayList<BluetoothDevice> = ArrayList()
      if (!pairedDevices.isEmpty()) {
        for (device: BluetoothDevice in pairedDevices) {
          list.add(device)
          Log.d(TAG, "add device: ${device.name}")
        }
      }
      if (!it.isDisposed) {
        it.onSuccess(list)
      }
    }
  }


}