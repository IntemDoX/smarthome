package com.intemdox.smarthome.repository

import android.app.Application
import android.arch.lifecycle.LiveData
import com.intemdox.smarthome.database.AppDatabase
import com.intemdox.smarthome.entity.BTDeviceEntity
import com.intemdox.smarthome.dao.IBTDevicesDao
import javax.inject.Inject

class BTDevicesRepository @Inject constructor(application: Application) {
  private var bTDevicesDao: IBTDevicesDao
  var allBtDevices: LiveData<List<BTDeviceEntity>>

  init {
    val db: AppDatabase? = AppDatabase.getDatabase(application)
    bTDevicesDao = db!!.provideBTDeviceDao()
    allBtDevices = bTDevicesDao.getAllDevices()
  }

  fun insert(btDeviceEntity: BTDeviceEntity) {
    //todo TO NEW THREAD
    bTDevicesDao.insert(btDeviceEntity)
  }

}