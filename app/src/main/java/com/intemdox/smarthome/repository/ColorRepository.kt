package com.intemdox.smarthome.repository

import android.app.Application
import android.arch.lifecycle.LiveData
import android.util.Log
import com.intemdox.smarthome.entity.ColorEntity
import com.intemdox.smarthome.dao.IColorDao
import com.intemdox.smarthome.database.AppDatabase
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ColorRepository @Inject constructor(application: Application) {
  private var colorDao: IColorDao
  var savedColors: LiveData<List<ColorEntity>>
  var recentColors: LiveData<List<ColorEntity>>

  init {
    val db: AppDatabase? = AppDatabase.getDatabase(application)
    colorDao = db!!.provideColorDao()
    savedColors = colorDao.getSavedColors()
    recentColors = colorDao.getRecentColors()
  }

  fun insert(colorEntity: ColorEntity) {
    Completable.create({
      colorDao.insert(colorEntity)
      it.onComplete()
    }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({
      Log.d("testtest","YES!!!")
    })
  }
}