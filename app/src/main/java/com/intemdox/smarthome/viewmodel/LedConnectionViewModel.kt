package com.intemdox.smarthome.viewmodel

import android.app.Application
import android.arch.lifecycle.*
import android.bluetooth.BluetoothDevice
import android.util.Log
import com.intemdox.smarthome.BluetoothHelper
import com.intemdox.smarthome.entity.BTDeviceEntity
import com.intemdox.smarthome.dagger2.components.DaggerIBluetoothComponent
import com.intemdox.smarthome.dagger2.components.DaggerIRepositoryComponent
import com.intemdox.smarthome.dagger2.modules.ApplicationModule
import com.intemdox.smarthome.repository.BTDevicesRepository
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.NonNull
import io.reactivex.schedulers.Schedulers

class LedConnectionViewModel(application: Application) : AndroidViewModel(application){
    private var btHelper: BluetoothHelper
    var btDevicesMediator: MediatorLiveData<List<BTDeviceEntity>> = MediatorLiveData()
    var btDevicesRepository: BTDevicesRepository
    var btDevicesLiveData: LiveData<List<BTDeviceEntity>>

    init {
        val btRepositoryComponentComponent = DaggerIRepositoryComponent.builder().applicationModule(ApplicationModule(application)).build()
        val btComponent = DaggerIBluetoothComponent.builder().applicationModule(ApplicationModule(application)).build()
        btDevicesRepository = btRepositoryComponentComponent.getBTDeviceRepository()
        btDevicesLiveData = btDevicesRepository.allBtDevices
        btHelper = btComponent.getBluetoothHelper()
    }

    fun insert(btDeviceEntity: BTDeviceEntity) {
        btDevicesRepository.insert(btDeviceEntity)
    }

    fun findDeviceAction (isAdapterExist: Boolean) {
        if (isRefresh(isAdapterExist))
            refreshDevices()
        else
            findDevices()
    }

    fun connectToAllDevices(): Completable {
        return btHelper.connectToAllDevices()
    }

    fun connectToDevice(address: String) : Completable{
        return btHelper.connectToDevice(address)
    }

    fun isRefresh(isAdapterExist: Boolean): Boolean {
        return btHelper.isAdapterEnable() && isAdapterExist
    }


    fun refreshDevices(){
        btHelper.getPairedDeviceList()
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({
                    if (it != null) {
                        val entityList: MutableList<BTDeviceEntity> = ArrayList()
                        for(bluetoothDevice:BluetoothDevice in it) {
                            val value = BTDeviceEntity()
                            value.name = bluetoothDevice.name
                            value.macAddress = bluetoothDevice.address
                            entityList.add(value)
                        }
                        btDevicesMediator.value = entityList
                    }
                }, {
                   Log.d("testtest", "error")
                })
    }

    fun findDevices() : Completable{
        return btHelper.enableBT()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }


    class Factory(application: Application): ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return LedConnectionViewModel(application) as T
        }

        @NonNull
        var application : Application = application

    }

}