package com.intemdox.smarthome.viewmodel

import android.app.Application
import android.arch.lifecycle.*
import android.graphics.Color
import android.util.Log
import com.intemdox.smarthome.BluetoothHelper
import com.intemdox.smarthome.entity.ColorEntity
import com.intemdox.smarthome.dagger2.components.DaggerIBluetoothComponent
import com.intemdox.smarthome.dagger2.components.DaggerIRepositoryComponent
import com.intemdox.smarthome.dagger2.modules.ApplicationModule
import com.intemdox.smarthome.repository.ColorRepository
import io.reactivex.annotations.NonNull

class LedViewModel(application: Application) : AndroidViewModel(application) {

  var colorRepository: ColorRepository
  var savedColorsLiveData: LiveData<List<ColorEntity>>
  var savedColorsMediator: MediatorLiveData<List<ColorEntity>> = MediatorLiveData()
  var recentColorsLiveData: LiveData<List<ColorEntity>>
  var recentColorsMediator: MediatorLiveData<List<ColorEntity>> = MediatorLiveData()
  var btHelper: BluetoothHelper

  init {
    val colorRepositoryComponent = DaggerIRepositoryComponent.builder().applicationModule(ApplicationModule(application)).build()
    colorRepository = colorRepositoryComponent.getColorRepository()
    savedColorsLiveData = colorRepository.savedColors
    recentColorsLiveData = colorRepository.recentColors
    val btComponent = DaggerIBluetoothComponent.builder().applicationModule(ApplicationModule(application)).build()
    btHelper = btComponent.getBluetoothHelper()
  }

  fun saveColor(colorEntity: ColorEntity) {
    if ((colorEntity.type == 0 && colorRepository.savedColors.value != null && !colorRepository.savedColors.value!!.contains(colorEntity))
        || (colorEntity.type == 1 && colorRepository.recentColors.value != null && !colorRepository.recentColors.value!!.contains(colorEntity))) {
      colorRepository.insert(colorEntity)
    } else {
      Log.d("testtest", "color was already added")
    }
  }

  fun acceptColor(colorEntity: ColorEntity, isSaveColor: Boolean) {
    val r = Color.red(colorEntity.color)
    val g = Color.green(colorEntity.color)
    val b = Color.blue(colorEntity.color)
    btHelper.sendCommand(intArrayOf(0, r, g, b)).subscribe({
      Log.d("testtest", "SUCCESS!!!")
    }, {

      Log.d("testtest", "ERROR:(!!!")

    })
    if (isSaveColor) {
      saveColor(colorEntity)
    }
  }

  class Factory(@NonNull var application: Application) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
      return LedViewModel(application) as T
    }

  }

}